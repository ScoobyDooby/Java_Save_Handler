/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.savehandler.exceptions;

/**
 * This exception should be thrown by implementations of SaveState when one of
 * the save-state operations fails due to some internal exception meaning that
 * the in-memory save-state would be in danger of being lost if the operation
 * continued. 
 * <p>
 * The SaveState implementation is responsible for making sure that
 * no changes are made to the in-memory save-state until all code that can throw
 * this error has been successfully executed.
 * <p>
 * @author Scooby
 */
public class SaveStateOperationFailedException extends Exception
{

    public SaveStateOperationFailedException(String message)
    {
        super(message);
    }

    public SaveStateOperationFailedException()
    {
        super();
    }



}
