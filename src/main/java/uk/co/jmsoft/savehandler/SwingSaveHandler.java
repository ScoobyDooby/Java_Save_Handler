/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.savehandler;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.NAME;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 * Helper class for use with Java Swing programs.
 * <p>
 * A concrete implementation of AbstractSavehandler.
 * <p>
 * Provides an implementation of the typical new, open, save and save-as
 * operations
 * and their GUI dialogs.<br>
 * Uses a standard JFileChooser dialog to present the typical open and save-as
 * dialogs.
 * Requires that a JFrame be the parent window for all the dialogs.
 * <p>
 * Works in conjunction with an implementation of the SaveStateManager
 * interface, which must be passed as an argument to this classes constructor.
 * <p>
 * @author Scooby
 */
public class SwingSaveHandler extends AbstractSaveHandler
{

    private Component dialogParent;
    private JFrame mainWindow;
    private JFileChooser fileChooser = new JFileChooser();

    /**
     *
     * @param manager
     * @param dialogParent
     * @param mainWindow   - must be set to the main window JFrame in order to
     *                     handle unsaved changes on window closing.
     */
    public SwingSaveHandler(SaveStateManager manager, Component dialogParent, JFrame mainWindow, String preferenceNodeName)
    {
        super(manager, preferenceNodeName);
        setCurrentDirectory(new File(Preferences.userRoot().node(preferenceNodeName).get(currentDirectoryPreference, System.getProperty("user.home"))));
        this.dialogParent = dialogParent;
        this.mainWindow = mainWindow;
    }

    /**
     * Sets the component that the save, open and warning dialogs will have as
     * their parent.
     * <p>
     * @param dialogParent
     */
    public void setDialogParent(Component dialogParent)
    {
        this.dialogParent = dialogParent;
    }

    /**
     * Sets the JFrame object that will trigger unsaved changes handling on
     * close.
     * <p>
     * @param mainWindow
     */
    public void setMainWindowFrame(JFrame mainWindow)
    {
        this.mainWindow = mainWindow;
    }

    /**
     * Sets whether the JFileChooser load/save dialog will show the show all
     * files option.
     * <p>
     * @param accept
     */
    public void setAcceptAllFileFilterEnabled(boolean accept)
    {
        fileChooser.setAcceptAllFileFilterUsed(accept);
    }

    /**
     * Sets a specific file filter for the load and save dialogs to use.
     * <p>
     * @param filter
     */
    public void setFileFilter(FileFilter filter)
    {
        fileChooser.setFileFilter(filter);
    }

    /**
     * Sets the component that the open, save and warning dialogs will use.
     * <p>
     * @param dialogParent
     */
    public void setFileChooserParent(Component dialogParent)
    {
        this.dialogParent = dialogParent;
    }

    /**
     * Set a custom file chooser to replace the default one.
     * <p>
     * @param fileChooser
     */
    public void setFileChooser(JFileChooser fileChooser)
    {
        this.fileChooser = fileChooser;
    }

    /**
     * Returns the JFileChooser object being used for open and save operations.
     * <p>
     * @return
     */
    public JFileChooser getFileChooser()
    {
        return fileChooser;
    }

    /**
     * Returns a list of javax.swing.Action objects corresponding to the list of
     * recentFiles.
     * These can be added to GUI components such as buttons and menu items to
     * trigger loading of the files.
     * <p>
     * @return
     */
    public List<Action> getRecentFileActions()
    {
        List<Action> result = new ArrayList<>();
        for (String eachFile : getRecentFilePathsList())
        {
            result.add(new OpenRecentAction(eachFile));
        }
        return result;
    }

    public class OpenRecentAction extends AbstractAction
    {

        public OpenRecentAction(String name)
        {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                open(new File(this.getValue(NAME).toString()));
                publishOpenRecentFileEvent();
            }
            catch (SaveStateOperationFailedException ex)
            {
                Logger.getLogger(SwingSaveHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Returns a list of JMenuItem objects corresponding to the recent file
     * list.
     * They have been initialised with Actions that set the name of the
     * JMenuItem to the name of the file.
     * The action triggers loading of the associated file.
     * <p>
     * @return
     */
    public List<JMenuItem> getRecentFileJMenuItems()
    {
        List<JMenuItem> result = new ArrayList<>();
        for (Action eacAction : getRecentFileActions())
        {
            result.add(new JMenuItem(eacAction));
        }
        return result;
    }

    private SaveHandlerModel.DialogOption jOptionPaneToDialogOption(int option)
    {
        if (option == JOptionPane.OK_OPTION)
        {
            return SaveHandlerModel.DialogOption.CONFIRM;
        }
        if (option == JOptionPane.NO_OPTION)
        {
            return SaveHandlerModel.DialogOption.DENY;
        }
        if (option == JOptionPane.CANCEL_OPTION)
        {
            return SaveHandlerModel.DialogOption.CANCEL;
        }
        // Should never happen
        throw new IllegalStateException("JOptionPane returned and invalid option code.");
    }

    private SaveHandlerModel.DialogOption jFileChooserToDialogOption(int option)
    {
        if (option == JFileChooser.APPROVE_OPTION)
        {
            return SaveHandlerModel.DialogOption.CONFIRM;
        }
        if (option == JFileChooser.CANCEL_OPTION)
        {
            return SaveHandlerModel.DialogOption.CANCEL;
        }
        // Should never happen.
        throw new IllegalStateException("JFileChooser returned an invalid option code.");
    }

    @Override
    protected void showSaveSucceededMessageBox()
    {
        JOptionPane.showMessageDialog(dialogParent, saveSuccessfullMessage, saveSuccessfullTitle, JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    protected void showOpenSucceededMessageBox()
    {
        JOptionPane.showMessageDialog(dialogParent, loadSuccessMessage, loadSuccessTitle, JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void showFailedToSaveMessageBox()
    {
        JOptionPane.showMessageDialog(dialogParent, failedToSaveMessage, failedToSaveTitle, JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void showFailedToOpenMessageBox()
    {
        JOptionPane.showMessageDialog(dialogParent, failedToLoadMessage, failedToLoadTitle, JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void showFailedToCreateNewMessageBox()
    {
        JOptionPane.showMessageDialog(dialogParent, failedToCreateNewMessage, failedToCreateNewTitle, JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public SaveHandlerModel.DialogOption showOverwriteWarningDialog()
    {
        int option = JOptionPane.showConfirmDialog(dialogParent, fileAlreadyExistsMessage, fileAlreadyExistsTitle, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        return jOptionPaneToDialogOption(option);
    }

    @Override
    public SaveHandlerModel.DialogOption showUnsavedChangesDialog()
    {
        int option = JOptionPane.showConfirmDialog(dialogParent, unsavedChangeMessage);
        return jOptionPaneToDialogOption(option);
    }

    @Override
    public SaveHandlerModel.DialogOption showSaveDialog()
    {
        int option = fileChooser.showSaveDialog(dialogParent);
        return jFileChooserToDialogOption(option);
    }

    @Override
    public SaveHandlerModel.DialogOption showOpenDialog()
    {
        int option = fileChooser.showOpenDialog(dialogParent);
        return jFileChooserToDialogOption(option);
    }

    @Override
    public File getSelectedFile()
    {
        return fileChooser.getSelectedFile();
    }

    @Override
    public void setDefaultCloseOperation(SaveHandlerModel.CloseOperation code)
    {
        switch (code)
        {
            case DO_NOTHING:
                mainWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                break;
            case EXIT:
                mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                break;
            default:
                mainWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                JOptionPane.showMessageDialog(dialogParent, "A program logic error occured when trying to set the window close behaviour. The window will not close.", "Error", JOptionPane.ERROR_MESSAGE);
                break;
            //Should never happen but dont exit just in case
        }
    }

    @Override
    public void setCurrentDirectory(File directory)
    {
        if (directory.isDirectory())
        {
            fileChooser.setCurrentDirectory(directory);
        }
    }

    @Override
    public File getCurrentDirectory()
    {
        return fileChooser.getCurrentDirectory();
    }

}
