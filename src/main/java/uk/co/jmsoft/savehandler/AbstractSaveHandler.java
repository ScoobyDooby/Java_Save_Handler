/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.savehandler;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 * Abstract base class that provides a 'GUI framework independent' interface for
 * client code interaction with the save handler. An extension of this class
 * must be created specifically for each GUI framework the save handler
 * framework is to be used with (eg. for Swing or JavaFX) to provide the
 * code specific to the GUI framework.
 * <p>
 * @author Scooby
 */
public abstract class AbstractSaveHandler
{

    private final Set<OpenRecentFileListener> openRecentFileListeners = new HashSet<>();
    private final Set<RecentFilesUpdateListener> recentFilesUpdateListeners = new HashSet<>();
    // Preferences

    protected final String currentDirectoryPreference = "currentDirectory";
    protected String preferenceNodeName;
    protected final String maxRecentFilesPreference = "maxRecentFilesPreference";
    protected final int defaultMaxRecentFiles = 10;
    protected final String recentPrefix = "recent";
    // End Preferences

    boolean handleRecentFiles = false;
    protected SaveStateManager saveStateManager;

    private final SaveHandlerModel saveHandlerModel;

    protected Object unsavedChangeMessage = "There are unsaved changes. Do you want to save before continuing?";

    protected Object failedToSaveMessage = "Failed to save!";
    protected String failedToSaveTitle = "Failed to Save!";

    protected Object failedToLoadMessage = "Failed to load!";
    protected String failedToLoadTitle = "Failed to load!";

    protected Object fileAlreadyExistsMessage = "File already exists. Saving will overwrite this file. Do you want to continue?";
    protected String fileAlreadyExistsTitle = "File already exists";

    protected Object failedToCreateNewMessage = "Failed to create new save!";
    protected String failedToCreateNewTitle = "Failed to create new save!";

    protected Object saveSuccessfullMessage = "Save was successful.";
    protected String saveSuccessfullTitle = "Save was successful.";

    protected Object loadSuccessMessage = "Load was successful.";
    protected String loadSuccessTitle = "Load was successful.";

    public AbstractSaveHandler(SaveStateManager saveStateManager, String preferenceNodeName)
    {
        //setCurrentDirectory(new File(Preferences.userRoot().node(preferenceNodeName).get(currentDirectoryPreference, System.getProperty("user.home"))));
        this.preferenceNodeName = preferenceNodeName;
        this.saveStateManager = saveStateManager;
        this.saveHandlerModel = new SaveHandlerModel(saveStateManager, this);
    }

    protected abstract void showSaveSucceededMessageBox();

    protected abstract void showOpenSucceededMessageBox();

    protected abstract void showFailedToSaveMessageBox();

    protected abstract void showFailedToOpenMessageBox();

    protected abstract void showFailedToCreateNewMessageBox();

    protected abstract SaveHandlerModel.DialogOption showOverwriteWarningDialog();

    protected abstract SaveHandlerModel.DialogOption showUnsavedChangesDialog();

    protected abstract SaveHandlerModel.DialogOption showSaveDialog();

    protected abstract SaveHandlerModel.DialogOption showOpenDialog();

    protected abstract File getSelectedFile();

    protected abstract void setDefaultCloseOperation(SaveHandlerModel.CloseOperation code);

    public void addOpenRecentFileListener(OpenRecentFileListener listener)
    {
        openRecentFileListeners.add(listener);
    }

    public void removeOpenRecentFileListener(OpenRecentFileListener listener)
    {
        openRecentFileListeners.remove(listener);
    }

    protected void publishOpenRecentFileEvent()
    {
        for (OpenRecentFileListener listener : openRecentFileListeners)
        {
            listener.openRecent();
        }
    }

    

    /**
     * Sets a listener for recent file update events.
     * <p>
     * @param listener
     */
    public void addRecentFilesUpdateListener(RecentFilesUpdateListener listener)
    {
        this.recentFilesUpdateListeners.add(listener);
    }

    public void removeRecentFilesUpdateListener(RecentFilesUpdateListener listener)
    {
        recentFilesUpdateListeners.remove(listener);
    }

    private void publishRecentFileUpdateEvent()
    {
        for (RecentFilesUpdateListener eachListener : recentFilesUpdateListeners)
        {
            eachListener.update();
        }
    }

    /**
     * Sets whether a recent files list should be maintained.
     * Default is false;
     * If set false any existing list is retained.
     * <p>
     * @param handleRecentFiles
     */
    public void setHandleRecentFiles(boolean handleRecentFiles)
    {
        this.handleRecentFiles = handleRecentFiles;
    }

    /**
     * Returns the recent files paths as a list of Strings.
     * <p>
     * @return
     */
    public List<String> getRecentFilePathsList()
    {
        List<String> result = new ArrayList<>();

        Preferences preferences = getPreferences();
        int maxRecentFiles = preferences.getInt(maxRecentFilesPreference, defaultMaxRecentFiles);

        for (int i = 0; i < maxRecentFiles; i++)
        {
            String path = preferences.get(recentPrefix + i, null);
            if (path != null)
            {
                result.add(path);
            }
        }
        return result;
    }

    /**
     * Sets the maximum number of recent files to remember. Default is 10.
     * <p>
     * @param number
     */
    public void setMaxRecentFiles(int number)
    {
        Preferences preferences = getPreferences();
        int currentNumber = preferences.getInt(maxRecentFilesPreference, defaultMaxRecentFiles);

        if (currentNumber > number)
        {
            // We need to delete some preferences
            // recent files are zero indexed
            for (int i = number; i < currentNumber; i++)
            {
                preferences.remove(recentPrefix + i);
            }
            publishRecentFileUpdateEvent();
        }
        preferences.putInt(maxRecentFilesPreference, number);

    }

    /**
     * Returns the maximum number of recent files to remember.
     * <p>
     * @return
     */
    public int getMaxRecentFiles()
    {
        return getPreferences().getInt(maxRecentFilesPreference, defaultMaxRecentFiles);
    }

    private Preferences getPreferences()
    {
        return Preferences.userRoot().node(preferenceNodeName);
    }

    protected void updateRecentFiles(String path)
    {
        Deque<String> recentFiles = new ArrayDeque<>();
        Preferences preferences = getPreferences();
        // Get recent files from preferences
        int maxRecentFiles = preferences.getInt(maxRecentFilesPreference, defaultMaxRecentFiles);
        for (int i = 0; i < maxRecentFiles; i++)
        {
            String tempPath = preferences.get(recentPrefix + i, null);
            if (tempPath != null)
            {
                recentFiles.addLast(tempPath);
            }
        }

        // Update recent files
        if (recentFiles.contains(path))
        {
            //Move file to top of list
            recentFiles.remove(path);
            recentFiles.addFirst(path);
        }
        else
        {
            recentFiles.addFirst(path);
            if (recentFiles.size() > maxRecentFiles)
            {
                recentFiles.removeLast();
            }
        }
        //Update preferences
        int numberOfFiles = recentFiles.size();
        for (int i = 0; i < numberOfFiles; i++)
        {
            preferences.put(recentPrefix + i, recentFiles.pop());
        }
        publishRecentFileUpdateEvent();
    }

    /**
     * Removes all recent files
     */
    public void clearRecentFiles()
    {
        Preferences preferences = getPreferences();
        int recentFilesNumber = preferences.getInt(maxRecentFilesPreference, defaultMaxRecentFiles);
        for (int i = 0; i < recentFilesNumber; i++)
        {
            preferences.remove(recentPrefix + i);
        }
        publishRecentFileUpdateEvent();
    }

    /**
     * Method for use in testing only. The client code should not have direct
     * access to the SaveHandlerModel.
     * <p>
     * @return
     */
    protected SaveHandlerModel getSaveHandlerModel()
    {
        return saveHandlerModel;
    }

    /**
     * Sets whether a confirmation message should be shown after saving.
     * Default is false;
     * <p>
     * @param showOpenSuccessMessage
     */
    public void setShowOpenSuccessMessage(boolean showOpenSuccessMessage)
    {
        saveHandlerModel.setShowOpenSuccessMessage(showOpenSuccessMessage);
    }

    /**
     * Sets whether a confirmation message should be shown after loading.
     * Default is false;
     * <p>
     * @param showSaveSuccessMessage
     */
    public void setShowSaveSuccessMessage(boolean showSaveSuccessMessage)
    {
        saveHandlerModel.setShowSaveSuccessMessage(showSaveSuccessMessage);
    }

    /**
     * Sets the message that will appear in the save was successful dialog.
     * The default message is "Load was successful."
     * <p>
     * @param loadSuccessMessage
     */
    public void setOpenSuccessMessage(Object loadSuccessMessage)
    {
        this.loadSuccessMessage = loadSuccessMessage;
    }

    /**
     *
     * Sets the title of the message box shown after successfully loading a
     * file.
     * Default is "Load was successful."
     * <p>
     * @param loadSuccessTitle
     */
    public void setOpenSuccessTitle(String loadSuccessTitle)
    {
        this.loadSuccessTitle = loadSuccessTitle;
    }

    /**
     * Sets the message that will appear in the load was successful
     * dialog box.
     * The default is "Save was successful."
     * <p>
     * @param saveSuccessfullMessage
     */
    public void setSaveSuccessfullMessage(Object saveSuccessfullMessage)
    {
        this.saveSuccessfullMessage = saveSuccessfullMessage;
    }

    /**
     * Sets the title of the message box shown after successfully saving a file.
     * Default is "Save was successful.";
     * <p>
     * @param saveSuccessfullTitle
     */
    public void setSaveSuccessfullTitle(String saveSuccessfullTitle)
    {
        this.saveSuccessfullTitle = saveSuccessfullTitle;
    }

    /**
     * Sets the message that will appear in the file already exists warning
     * dialog box.
     * The default message is "File already exists. Saving will overwrite this
     * file. Do you want to continue?"
     * <p>
     * @param fileAlreadyExistsMessage
     */
    public void setFileAlreadyExistsMessage(Object fileAlreadyExistsMessage)
    {
        this.fileAlreadyExistsMessage = fileAlreadyExistsMessage;
    }

    /**
     * Sets the title of the message box shown when saving will overwrite an
     * existing file.
     * Default is "File already exists";
     * <p>
     * @param fileAlreadyExistsTitle
     */
    public void setFileAlreadyExistsTitle(String fileAlreadyExistsTitle)
    {
        this.fileAlreadyExistsTitle = fileAlreadyExistsTitle;
    }

    /**
     * Sets the message that will appear in the unsaved changes dialog box.
     * The default message is "There are unsaved changes. Do you want to save
     * before continuing?"
     * <p>
     * @param unsavedChangeMessage
     */
    public void setUnsavedChangeMessage(Object unsavedChangeMessage)
    {
        this.unsavedChangeMessage = unsavedChangeMessage;
    }

    /**
     * Sets the message displayed in the message box shown when saving fails.
     * Default is "Failed to save!".
     * <p>
     * @param failedToSaveMessage
     */
    public void setFailedToSaveMessage(Object failedToSaveMessage)
    {
        this.failedToSaveMessage = failedToSaveMessage;
    }

    /**
     * Sets the title of the message box shown when saving fails.
     * Default is "Failed to load!";
     * <p>
     * @param failedToSaveTitle
     */
    public void setFailedToSaveTitle(String failedToSaveTitle)
    {
        this.failedToSaveTitle = failedToSaveTitle;
    }

    /**
     * Sets the message displayed in the message box shown when loading fails.
     * Default is "Failed to load!".
     * <p>
     * @param failedToLoadMessage
     */
    public void setFailedToLoadMessage(Object failedToLoadMessage)
    {
        this.failedToLoadMessage = failedToLoadMessage;
    }

    /**
     * Sets the title of the message box shown when loading fails.
     * Default is "Failed to save!";
     * <p>
     * @param failedToLoadTitle
     */
    public void setFailedToLoadTitle(String failedToLoadTitle)
    {
        this.failedToLoadTitle = failedToLoadTitle;
    }

    /**
     * Sets the message displayed in the message box shown when creating a new
     * save fails.
     * Default is "Failed to create new save!".
     * <p>
     * @param failedToCreateNewMessage
     */
    public void setFailedToCreateNewMessage(Object failedToCreateNewMessage)
    {
        this.failedToCreateNewMessage = failedToCreateNewMessage;
    }

    /**
     * Sets the title of the message box shown when starting a new save fails.
     * Default is "Failed to create new save!";
     * <p>
     * @param failedToCreateNewTitle
     */
    public void setFailedToCreateNewTitle(String failedToCreateNewTitle)
    {
        this.failedToCreateNewTitle = failedToCreateNewTitle;
    }

    /**
     * Sets the directory the file chooser is pointing to.
     * <p>
     * @param directory
     */
    public abstract void setCurrentDirectory(File directory);

    /**
     * Checks for unsaved changes and then loads the specified file.
     * <p>
     * @param file
     *             <p>
     * @return
     * @throws SaveStateOperationFailedException
     */
    public Result open(File file) throws SaveStateOperationFailedException
    {
        Result result = saveHandlerModel.open(file);
        return result;
    }

    /**
     * Checks for any unsaved changes and prompt the user with an option to save
     * them as for the save() method. Then displays the open file dialog and
     * then attempts to load the
     * selected file. The user may cancel the operation at any stage and the
     * in-memory save state will remain unchanged.
     * <p>
     * @return
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    public Result open() throws SaveStateOperationFailedException
    {
        Result result = saveHandlerModel.open();
        return result;
    }

    /**
     * Attempts to save the current in-memory save-state of the SaveStateManager
     * to an external file quietly.
     * <p>
     * If the current in-memory save-state has no
     * associated external file the user will be prompted with the save-as
     * dialog.
     * <p>
     * If saving fails the user is notified with a message and an exception is
     * thrown to notify the GUI.
     * <p>
     * @return
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    public Result save() throws SaveStateOperationFailedException
    {
        Result result = saveHandlerModel.save();
        return result;
    }

    /**
     * Shows the save dialog.
     * <p>
     * If the file fails to save the user is notified with a message dialog and
     * an exception is thrown to notify the GUI.
     * <p>
     * <p>
     * @return
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    public Result saveAs() throws SaveStateOperationFailedException
    {
        Result result = saveHandlerModel.saveAs();
        return result;
    }

    /**
     * Checks for any unsaved changes and prompts the user to decide whether to
     * save them or not.
     * <p>
     * @return
     *         <p>
     * @throws SaveStateOperationFailedException
     */
    public Result checkForUnsavedChanges() throws SaveStateOperationFailedException
    {
        return saveHandlerModel.checkForUnsavedChanges();
    }

    /**
     * Checks for unsaved changes and prompts the user with the option to save
     * them. If the save is successful the save-state is reset to its
     * blank/default/new state. Failure results in a message dialog to inform
     * the user.
     * <p>
     * @return
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    public Result newSaveState() throws SaveStateOperationFailedException
    {
        return saveHandlerModel.newSaveState();
    }

    /**
     * Checks for unsaved changes and prompts the user to either save or discard
     * them. Returns the JOptionPane option code selected by the user or the
     * JOptionPane.OK_OPTION code if there were no unsaved changes.
     * <p>
     * @return
     * @throws SaveStateOperationFailedException
     */
    public Result handleWindowClosing() throws SaveStateOperationFailedException
    {
        Preferences.userRoot().node(preferenceNodeName).put(currentDirectoryPreference, getCurrentDirectory().getPath());
        return saveHandlerModel.handleWindowClosing();
    }

    /**
     * Returns the path to the directory the file chooser is currently pointing
     * at.
     * <p>
     * @return
     */
    public abstract File getCurrentDirectory();

}
