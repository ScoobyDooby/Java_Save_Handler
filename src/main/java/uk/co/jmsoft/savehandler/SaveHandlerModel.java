/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.savehandler;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 * The model-side backend for the SaveHandlerView class.
 * <p>
 * Encapsulates the unsaved changes logic.
 * <p>
 * @author Scooby
 */
class SaveHandlerModel
{

    public enum DialogOption
    {

        CONFIRM,
        DENY,
        CANCEL
    }

    public enum CloseOperation
    {

        DO_NOTHING,
        EXIT
    }

    private final SaveStateManager saveState;
    private final AbstractSaveHandler saveHandler;

    private boolean showSaveSuccessMessage = false;
    private boolean showOpenSuccessMessage = false;

    protected SaveHandlerModel(SaveStateManager saveState, AbstractSaveHandler saveHandler)
    {
        this.saveHandler = saveHandler;
        this.saveState = saveState;
    }

    public void setShowOpenSuccessMessage(boolean showOpenSuccessMessage)
    {
        this.showOpenSuccessMessage = showOpenSuccessMessage;
    }

    public void setShowSaveSuccessMessage(boolean showSaveSuccessMessage)
    {
        this.showSaveSuccessMessage = showSaveSuccessMessage;
    }

    protected Result tryToSave(File file) throws SaveStateOperationFailedException
    {
        try
        {
            saveState.save(file);
            saveHandler.updateRecentFiles(file.getPath());
            if (showSaveSuccessMessage)
            {
                saveHandler.showSaveSucceededMessageBox();
            }
            return Result.SUCCESS;
        }
        catch (SaveStateOperationFailedException ex)
        {
            Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.WARNING, "Failed to save.", ex);
            saveHandler.showFailedToSaveMessageBox();
            throw ex;
        }
    }

    protected Result save() throws SaveStateOperationFailedException
    {
        if (saveState.isSavedExternally())
        {
            return tryToSave(saveState.getExternalFile());
        }
        else
        {
            return saveAs();
        }
    }

    protected Result saveAs() throws SaveStateOperationFailedException
    {
        boolean done = false;
        while (true)
        {
            DialogOption option = saveHandler.showSaveDialog();
            switch (option)
            {
                case CONFIRM:
                {
                    // Check for overwriting existing file.
                    if (saveHandler.getSelectedFile().exists())
                    {
                        switch (saveHandler.showOverwriteWarningDialog())
                        {
                            case CONFIRM:
                                return tryToSave(saveHandler.getSelectedFile());

                            case DENY:
                                //Show the save file chooser dialog again.
                                break;

                            case CANCEL:
                                return Result.CANCELLED;

                            default:
                                //Should never happen
                                Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.SEVERE, "Invalid JOptionPane input type.");
                                throw new IllegalStateException("Invalid JOptionPane input type.");
                        }
                    }
                    else // file doesn't exist
                    {
                        try
                        {
                            saveState.save(saveHandler.getSelectedFile());
                            if (showSaveSuccessMessage)
                            {
                                saveHandler.showSaveSucceededMessageBox();
                            }
                            return Result.SUCCESS;
                        }
                        catch (SaveStateOperationFailedException ex)
                        {
                            Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.WARNING, "Failed to saveAs.", ex);
                            saveHandler.showFailedToSaveMessageBox();
                            throw ex;
                        }
                    }
                }
                break;

                case CANCEL:
                    return Result.CANCELLED;

                default:
                    //Should never happen
                    Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.SEVERE, "Invalid option code from save dialog request. Code: {0}", option);
                    throw new IllegalStateException("Save dialog request returned an invalid option code: " + option.toString());

            }
        }
    }

    protected Result tryToOpen(File file) throws SaveStateOperationFailedException
    {
        try
        {
            saveState.open(file);
            saveHandler.updateRecentFiles(file.getPath());
            if (showOpenSuccessMessage)
            {
                saveHandler.showOpenSucceededMessageBox();
            }
            return Result.SUCCESS;
        }
        catch (SaveStateOperationFailedException ex)
        {
            Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.WARNING, "Failed to open file: " + file.getPath(), ex);
            saveHandler.showFailedToOpenMessageBox();
            throw ex;
        }
    }

    protected Result open(File file) throws SaveStateOperationFailedException
    {
        Result result;
        try
        {
            result = checkForUnsavedChanges();

            if (result == Result.CANCELLED)
            {
                return result;
            }

            return tryToOpen(file);

        }
        catch (SaveStateOperationFailedException ex)
        {
            // Saving unsaved changes failed caught here to avoid showing load failed message.
            throw ex;
        }
    }

    protected Result open() throws SaveStateOperationFailedException
    {
        Result result = checkForUnsavedChanges();
        if (result == Result.CANCELLED)
        {
            return result;
        }
        
        DialogOption option = saveHandler.showOpenDialog();
        switch (option)
        {
            case CONFIRM:
                return tryToOpen(saveHandler.getSelectedFile());
                
            case CANCEL:
                return Result.CANCELLED;
                
            default:
                //Should never happen
                Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.SEVERE, "Invalid JFileChooser option code: {0}", option);
                throw new IllegalStateException("Invalid JFileChooser option code: " + option);
        }
    }

    protected Result newSaveState() throws SaveStateOperationFailedException
    {
        try
        {
            Result result = checkForUnsavedChanges();
            if (result == Result.SUCCESS)
            {
                saveState.newSaveState();
            }
            return result;
        }
        catch (SaveStateOperationFailedException ex)
        {
            Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.SEVERE, null, ex);
            saveHandler.showFailedToCreateNewMessageBox();
            throw ex;
        }
    }

    protected Result checkForUnsavedChanges() throws SaveStateOperationFailedException
    {
        if (saveState.hasUnsavedChanges())
        {
            DialogOption option = saveHandler.showUnsavedChangesDialog();
            switch (option)
            {
                case CONFIRM:
                    save();
                    return Result.SUCCESS;

                case DENY:
                    return Result.SUCCESS;

                case CANCEL:
                    return Result.CANCELLED;
                default:
                    //Should not happen
                    Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.SEVERE, "Invalid option code when checking for unsaved changes: {0}", option);
                    System.exit(0);
            }
        }
        return Result.SUCCESS;
    }

    /**
     * This method can be optionally called from the JFrames formWindowClosing()
     * event handler. <br>Checks for unsaved changes and prompts the user as to
     * whether they want to save the changes.
     * <br>If an error occurs while trying to save the window will not be closed
     * and
     * the user will be show a warning message.
     * <p>
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    protected Result handleWindowClosing() throws SaveStateOperationFailedException
    {
        try
        {
            Result result = checkForUnsavedChanges();
            if (result == Result.CANCELLED)
            {
                saveHandler.setDefaultCloseOperation(CloseOperation.DO_NOTHING);
            }
            else
            {
                saveHandler.setDefaultCloseOperation(CloseOperation.EXIT);
            }
            return result;
        }
        catch (SaveStateOperationFailedException ex)
        {
            Logger.getLogger(SaveHandlerModel.class.getName()).log(Level.WARNING, "Failed to save on window closing. Window will not close.", ex);
            saveHandler.setDefaultCloseOperation(CloseOperation.DO_NOTHING);
            throw ex;
        }
    }

}
