/**
 * <h1>Java Save Handler</h1>
 * An abstract framework and concrete Swing GUI implementation that encapsulates
 * an applications typical save-state operations such as save, saveAs, open,
 * new, checking for unsaved changes and maintaining a list of recent files.<p>
 * <h2>Features</h2>
 * <ul>
 * <li>Handles all the conventional logic for new, open, save, save-as, check
 * for unsaved
 * changes operations.</li>
 * <li>Handles unsaved changes and load/save exception logic and orchestrates
 * all corresponding GUI dialog and message box calls. Your program simply
 * creates an instance of the appropriate SaveHandler class and implements the
 * SaveStateManager interface.</li>
 * <li>Handles recent file list using the Java Preferences API
 * <ul>
 * <li>The maximum number of files to remember is settable.</li>
 * <li>Swing implementation provides automated creation of recent File
 * JMenuItems and Actions</li>
 * </ul>
 * <br>
 * <li>Concrete implementation SwingSaveHandler provides all conventional Swing
 * dialogs. AbstractSaveHandler can be extended
 * for use with other GUI frameworks
 * <ul>
 * <li>SwingSaveHandler provides easy customisation of dialog and message box
 * messages.</li>
 * </ul>
 * </ul>
 */
package uk.co.jmsoft.savehandler;
