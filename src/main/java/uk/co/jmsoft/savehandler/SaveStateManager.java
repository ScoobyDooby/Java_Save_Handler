/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.savehandler;

import java.io.File;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 * Classes that implement this interface are responsible for maintaining the
 * programs in-memory save-state and also for saving it to its corresponding
 * external file.
 * <p>
 * In the Swing Model-View separation approach this interface will be
 * implemented by a model-side class. This interface is intended to be used in
 * conjunction with an instance of AbstractSaveHandler which encapsulates the
 * View-side of saving and the unsaved changes logic. Its methods <b>should
 * not</b> be called directly by client code unless you wish to entirely bypass
 * the save handler framework.
 * <p>
 * Note that to ensure that data is not lost in case of a problem while saving,
 * it is the responsibility of the implementation to ensure that the in-memory
 * save-state is not changed until all code that can throw exceptions has
 * successfully completed.
 * <br>
 * A SaveStateManager is also responsible for keeping track of any unsaved
 * changes.
 * <p>
 * @author Scooby
 */
public interface SaveStateManager
{

    /**
     * Returns whether the current in-memory save-state has been changed since
     * it was created or loaded from an external file.
     * <p>
     * @return
     */
    boolean hasUnsavedChanges();

    /**
     * Instructs the SaveFileSource to save the current in-memory save-state to
     * the specified external file.
     * <p>
     * @param file
     *             <p>
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    void save(File file) throws SaveStateOperationFailedException;

    /**
     * Returns whether the save-state in memory is associated with an external
     * file.
     * <p>
     * @return
     */
    boolean isSavedExternally();

    /**
     * Requests the File object that represents the external file that is
     * associated with the current in-memory save-state. Return null if there is
     * no such external file yet.
     * <p>
     * @return
     */
    File getExternalFile();

    /**
     * Requests that the in-memory save state be reset to the default/blank/new
     * state.
     * <p>
     * Note that ordinarily the new in-memory save-state <b>should not</b>
     * be associated with an external file to avoid accidentally overwriting it.
     * <p>
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    void newSaveState() throws SaveStateOperationFailedException;

    /**
     * Requests that the specified external file is loaded into the in-memory
     * save-state.
     * <p>
     * Implementations must preserve the current in-memory save-state in the
     * case
     * of any Exceptions being thrown so that no data is lost and so that the
     * program can continue.
     * <p>
     * @param file
     *             <p>
     * @throws uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException
     */
    void open(File file) throws SaveStateOperationFailedException;
}
